﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Note> notesList = new List<Note>();
            UserInteraction.MainMenu(notesList);
        }

        public static void AddNote(List<Note> notesList)
        {
            Console.Clear();
            Note note = new Note();
            Console.WriteLine("Поля, помеченные звёздочкой (*) являются обязательными.\n");
            Console.Write("Введите Фамилию*: ");
            note.Surname = Console.ReadLine();
            Console.Write("Введите Имя*: ");
            note.Name = Console.ReadLine();
            Console.Write("Введите Отчество: ");
            note.Patronymic = Console.ReadLine();
            Number:
            try
            {
                Console.Write("Введите номер телефона, только цифры*: ");
                note.NumPhone = Convert.ToInt64(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("\nНедопустимое значение. Попробуйте ещё разок.\n");
                goto Number;
            }
            Console.Write("Введите страну*: ");
            note.Country = Console.ReadLine();
            Console.Write("Введите дату рождения: ");
            note.BirthDate = Console.ReadLine();
            Console.Write("Введите организацию: ");
            note.Organization = Console.ReadLine();
            Console.Write("Введите должность: ");
            note.Position = Console.ReadLine();
            Console.Write("Прочие заметки: ");
            note.Comments = Console.ReadLine();
            notesList.Add(note);
            Console.WriteLine("\nЗамечательно! Ваша запись добавлена. \nДля перехода в главное меню нажмите Enter");
            Console.ReadLine();
        }

        public static void EditNote(List<Note> notesList)
        {
            if (notesList.Any())
            {

                ShowAllNotes(notesList);
                Console.WriteLine("Выберите, какой контакт Вы хотите отредактировать \n\nДля возврата в главное меню нажмите Enter\n");
                bool flag = true;
                while (flag)
                {
                    try
                    {
                        string choice = Console.ReadLine();
                        if (String.IsNullOrEmpty(choice)) { break; }
                        else if (Convert.ToInt32(choice) < 1 || Convert.ToInt32(choice) > notesList.Count)
                        {
                            Console.WriteLine("Такого контакта не существует");
                        }
                        //здесь я буду выводить список параметров, которые я хочу отредактировать: 
                        else
                        {
                            //вот здесь
                            Note noteToEdit = notesList[Convert.ToInt32(choice) - 1];
                            while (true)
                            {
                                ShowNoteProperties(noteToEdit); //вывожу одну запись
                                Console.WriteLine("\nВыберите, что Вы хотите изменить\nили нажмите Enter для выхода в главное меню");
                                string s = Console.ReadLine();
                                int chooseProperty;
                                //проверяю, чтобы в строку что-то ввелось
                                if (String.IsNullOrEmpty(s)) { break; }
                                else { chooseProperty = Convert.ToInt32(s); } //если ввелось, то превращаем в int

                                if (chooseProperty < 10 && chooseProperty > 0)
                                {
                                    Console.WriteLine("Введите новое значение:");
                                    string value = Console.ReadLine();
                                    UserInteraction.ChooseProperty(noteToEdit, chooseProperty, value);
                                }
                                else break;
                            }
                            flag = false;
                        }
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Не знаю, что Вы хотели этим сказать :С\nПопробуйте ещё разок!");
                        continue;
                    }
                }

            }
            else
            {
                Console.Clear();
                Console.WriteLine("В Вашей записной книжке нет ни одной записи!\n");
                Console.ReadLine();
            }
        }

        public static void DelNote(List<Note> notesList)
        {
            if (notesList.Any())
            {
                
                ShowAllNotes(notesList);
                Console.WriteLine("Выберите, какой контакт Вы хотите удалить \n\nДля возврата в главное меню нажмите Enter\n");
                bool flag = true;
                while (flag)
                {
                    try
                    {
                        string choice = Console.ReadLine();
                        if (String.IsNullOrEmpty(choice)) { break; }
                        else if (Convert.ToInt32(choice) < 1 || Convert.ToInt32(choice) > notesList.Count)
                        {
                            Console.WriteLine("Такого контакта не существует");
                        }
                        else
                        {
                            notesList.RemoveAt(Convert.ToInt32(choice) - 1);
                            Console.WriteLine("\nЗапись успешно удалена! \n\nДля возврата в главное меню введите Enter\n");
                            Console.ReadLine();
                            flag = false;
                        }
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Не знаю, что Вы хотели этим сказать :С\nПопробуйте ещё разок!");
                        continue;
                    }
                }
                
            }
            else
            {
                Console.Clear();
                Console.WriteLine("В Вашей записной книжке нет ни одной записи!\n");
                Console.ReadLine();
            }
        }

        public static void ReadNote(List<Note> notesList)
        {
            Console.Clear();
            if (!notesList.Any())
            {
                Console.WriteLine("В Вашей записной книжке нет ни одной записи!\n");
            }
            else
            {
                ShowNoteProperties(notesList.Last());
            }
            Console.WriteLine("Для возврата в главное меню нажмите Enter");
            Console.ReadLine();
        }

        public static void ShowAllNotes(List<Note> notesList)
        {
            Console.Clear();
            if (!notesList.Any())
            {
                Console.WriteLine("В Вашей записной книжке нет ни одной записи!\n");
            }
            else
            {
                Console.WriteLine("Список всех контактов:\n");
                int i = 0;
                foreach (Note n in notesList)
                {
                    i++;
                    Console.WriteLine($"{i}) {n.Surname} {n.Name} {n.NumPhone}\n");
                }
            }
        }
        public static void ShowAll(List<Note> notesList)
        {
            ShowAllNotes(notesList);
            Console.WriteLine("Для возврата в главное меню нажмите Enter");
            Console.ReadLine();
        }

        public static void ShowNoteProperties(Note note) //выводит все свойства объекта
        {
            Console.Clear();
            Console.WriteLine("1) Фамилия: " + note.Surname);
            Console.WriteLine("2) Имя: " + note.Name);
            Console.WriteLine("3) Отчество: " + note.Patronymic);
            Console.WriteLine("4) Номер телефона: " + note.NumPhone);
            Console.WriteLine("5) Страна: " + note.Country);
            Console.WriteLine("6) Дата рождения: " + note.BirthDate);
            Console.WriteLine("7) Организация: " + note.Organization);
            Console.WriteLine("8) Должность: " + note.Position);
            Console.WriteLine("9) Прочие заметки: " + note.Comments);
        }
    }
}
