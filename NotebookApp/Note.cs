﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Note
    {
        private string surname;
        private string name;
        private string country;
        public string Surname
        {
            get { return surname; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    Console.WriteLine("\nФамилия является обязательным полем!");
                    Surname = Console.ReadLine();
                }
                else
                {
                    surname = value;
                }
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    Console.WriteLine("\nИмя является обязательным полем!");
                    Name = Console.ReadLine();
                }
                else
                {
                    name = value;
                }
            }
        }
        public string Patronymic { get; set; }
        public long NumPhone { get; set; }
        public string Country
        {
            get { return country; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    Console.WriteLine("\nСтрана является обязательным полем!");
                    Country = Console.ReadLine();
                }
                else
                {
                    country = value;
                }
            }
        }
        public string BirthDate { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string Comments { get; set; }

    }
}
