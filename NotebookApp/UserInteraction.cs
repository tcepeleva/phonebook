﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class UserInteraction
    {
        public static void MainMenu(List<Note> notesList)
        {
            bool flag = true;
            while (flag)
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("Добро пожаловать в главное меню! Выберите, что Вы хотите сделать:");
                    Console.WriteLine("\n1 - Добавить новый контакт");
                    Console.WriteLine("2 - Редактировать контакт");
                    Console.WriteLine("3 - Удалить контакт");
                    Console.WriteLine("4 - Показать последнюю созданную запись");
                    Console.WriteLine("5 - Посмотреть все контакты");
                    Console.WriteLine("6 - Выйти из приложения");
                Start:
                    string choice = Console.ReadLine();
                    if (String.IsNullOrEmpty(choice))
                    {
                        goto Start;
                    }
                    switch (Convert.ToInt32(choice))
                    {
                        case 1:
                            Program.AddNote(notesList);
                            break;
                        case 2:
                            Program.EditNote(notesList);
                            break;
                        case 3:
                            Program.DelNote(notesList);
                            break;
                        case 4:
                            Program.ReadNote(notesList);
                            break;
                        case 5:
                            Program.ShowAll(notesList);
                            break;
                        case 6:
                            flag = false;
                            break;
                        default:
                            Console.WriteLine("\nЯ не запрограммирован на данную команду :С Попробуйте ещё разок");
                            goto Start;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Не знаю, что Вы хотели этим сказать :С\nПопробуйте ещё разок!");
                    Console.ReadKey();
                    continue;
                }
            }

        }

        public static void ChooseProperty(Note note, int choice, string value)
        {
            switch (choice)
            {
                case 1:
                    note.Surname = value;
                    break;
                case 2:
                    note.Name = value;
                    break;
                case 3:
                    note.Patronymic = value;
                    break;
                case 4:
                    note.NumPhone = Convert.ToInt64(value);
                    break;
                case 5:
                    note.Country = value;
                    break;
                case 6:
                    note.BirthDate = value;
                    break;
                case 7:
                    note.Organization = value;
                    break;
                case 8:
                    note.Position = value;
                    break;
                case 9:
                    note.Comments = value;
                    break;
                default:
                    break;
            }
        }
    }
}
